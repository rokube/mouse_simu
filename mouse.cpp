#include <iostream>
#include <vector>
#include <stdint.h>
#include <unistd.h>

#include "mouse.h"
#include "stopwatch.h"
#include "graph.h"

const long Mouse::STEP_TIME;

/*マウスの初期状態の設定*/
Mouse::Mouse(const Maze& new_maze,int16_t _maze_size){
  maze_size=_maze_size;
  coord.dir=NORTH;
  coord.x=0;
  coord.y=maze_size-1;
  maze=new Maze(new_maze);
}

void Mouse::sense_set_wall(const Maze& real_maze){
  if(sense_right(real_maze)){
    set_wall(trans_direction(Maze::dir_right));
  }
  if(sense_left(real_maze)){
    set_wall(trans_direction(Maze::dir_left)); 
  }
  if(sense_front(real_maze)){
    set_wall(trans_direction(Maze::dir_front)); 
  }
}

uint8_t Mouse::sense_front(const Maze& real_maze) const{
  uint8_t dir=NORTH;
  maze->setChkWall(coord,trans_direction(dir)<<4);
  return real_maze.getMazeData()[coord.y*maze_size+coord.x].wall&trans_direction(dir);
}

uint8_t Mouse::sense_right(const Maze& real_maze) const{
  uint8_t dir=EAST; 
  maze->setChkWall(coord,trans_direction(dir)<<4);
  return real_maze.getMazeData()[coord.y*maze_size+coord.x].wall&trans_direction(dir);
}

uint8_t Mouse::sense_left(const Maze& real_maze) const{
  uint8_t dir=WEST;
  maze->setChkWall(coord,trans_direction(dir)<<4);
  return real_maze.getMazeData()[coord.y*maze_size+coord.x].wall&trans_direction(dir);
}

uint8_t Mouse::move_forward(){
  coord.x+=!!(coord.dir&EAST)-!!(coord.dir&WEST);
  coord.y+=!!(coord.dir&SOUTH)-!!(coord.dir&NORTH);
  return 1;
}

void Mouse::turn_right(){
  coord.dir=(coord.dir<<1)&0x0F;
  if(coord.dir==0)coord.dir=NORTH;
}

void Mouse::turn_left(){
  coord.dir=(coord.dir>>1)&0x0F;
  if(coord.dir==0) coord.dir=WEST;
}

void Mouse::set_maze(const Maze& new_maze){
  delete maze;
  maze=new Maze(new_maze);
}

void Mouse::clear_maze(){
  int width=maze->getMazeSize();
  delete maze;
  maze=new Maze(width);
}

void Mouse::set_wall(uint8_t dir){
  maze->setWall(coord,dir);
}

uint8_t Mouse::trans_direction(const uint8_t local) const{
  uint8_t global=coord.dir;
  uint8_t dir=0;
  if(local==NORTH){
    return global; 
  }
  else if(local==EAST){
    dir=(((global&0x08)>>3)|(global<<1))&0x0F; 
    return dir;
  }
  else if(local==WEST){
    dir=(((global&0x01)<<3)|(global>>1))&0x0F;
    return dir;
  }
  else{
    std::cout<<"ouch"<<std::endl;
    return dir; 
  }
}

const Route Mouse::search_maze(Viewer* viewer,Maze& real_maze,uint8_t maze_w,int16_t start,int16_t goal,dijkstra_mode_t is_searching){
  int16_t index;
  Route route;
  StopWatch sw;

  while((index=get_coord_index())!=goal){
    Coord_t coord=get_coord();

    sw.start();
    Graph graph(getMaze());
    sw.stop();
    std::cout<<std::endl;
    std::cout<<"********************"<<std::endl<<"Graph Generation:";
    sw.show();

    sw.start();
    graph.dijkstra(goal,index,is_searching);
    route=graph.getRoute(goal,index);
    sw.stop();
    std::cout<<"********************"
      <<std::endl<<"Dijkstra Method:";
    sw.show();

    std::cout<<"********************"<<std::endl;
    for(Route::iterator it=route.begin()+1;it!=route.end();it++){
      std::cout<<std::endl<<index<<"->"<<(*it)<<std::endl;
      usleep(STEP_TIME);
      uint8_t route_dir=0;

      if((*it)==(index-maze_size)){
        //std::cout<<"NORTH:"<<route_dir<<std::endl;
        route_dir=NORTH; 
      } 
      else if((*it)==(index+1)){
        //std::cout<<"EAST"<<route_dir<<std::endl;
        route_dir=EAST; 
      } 
      else if((*it)==(index+maze_size)){
        //std::cout<<"SOUTH"<<route_dir<<std::endl;
        route_dir=SOUTH; 
      }
      else if((*it)==(index-1)){
        //std::cout<<"WEST"<<route_dir<<std::endl;
        route_dir=WEST; 
      }
      else{
        //std::cerr<<"wrong path. break"<<std::endl;
        return Route();
      }

      sense_set_wall(real_maze);

      std::cout<<"route:"<<route_dir<<"front:"<<
        trans_direction(Maze::dir_front)<<std::endl;
      viewer->append(route);
      viewer->append(getMaze());
      viewer->append(get_coord());
      viewer->show();

      if(route_dir==trans_direction(Maze::dir_front)){
        std::cout<<"direction route:front"<<std::endl;
        if(sense_front(real_maze)){
          std::cout<<"sensor detected:front"<<std::endl;
          break;
        }
        else{
          move_forward();
          usleep(STEP_TIME);
        }
      }
      else if(route_dir==trans_direction(Maze::dir_right)){
        std::cout<<"route:right"
          <<std::endl;
        if(sense_right(real_maze)){
          std::cout<<"sensor detected:right"
            <<std::endl;
          break;
        }
        else{
          turn_right();
          usleep(STEP_TIME);
          move_forward();
          usleep(STEP_TIME);
        }
      }
      else if(route_dir==trans_direction(Maze::dir_left)){
        std::cout<<"route:left"<<std::endl; 
        if(sense_left(real_maze)){
          std::cout<<"sensor detected:left"
            <<std::endl;
          break;
        }
        else{
          turn_left();
          usleep(STEP_TIME);
          move_forward();
          usleep(STEP_TIME);
        }
      }
      else{
        std::cout<<"route back"
          <<std::endl;
        turn_right();
        usleep(STEP_TIME);
        turn_right();
        usleep(STEP_TIME);
        move_forward();
        usleep(STEP_TIME);
      }
      index=get_coord_index();
      coord=get_coord();
    }
  }
  sense_set_wall(real_maze);
  return route;
}

void Mouse::get_unsearched_cell(std::vector<int16_t>& v,int16_t start,int16_t goal)const{
  v.clear();
  Graph graph=Graph(*maze);
  graph.dijkstra(goal,start,searching);
  const Route route=graph.getRoute(goal,start);
  std::cout<<"unsearched cells:"<<std::endl;
  for(Route::const_iterator it=route.begin();
      it!=route.end();it++){
    int16_t index=(*it);
    if(index!=start&&index!=goal&&!maze->is_searched_cell(index)){
      v.push_back(index);
      std::cout<<index<<":"<<maze->getMazeData()[index].wall
        <<std::endl;
    }
  }
}

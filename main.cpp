#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <cstdio>
#include <stdint.h>

#include "graph.h"
#include "maze.h"
#include "viewer.h"
#include "stopwatch.h"

int main(){
  int goal=135;   //ゴールの座標
  int start=240;  //スタートの座標

  int type;
  int maze_w,maze_h; //迷路の高さと長さ
  std::cout<<"Input Maze Data:"<<std::endl;
  std::cin>>type>>maze_w>>maze_h;

  Maze dat_maze(maze_w);    //読み込んだ迷路データを保存する.
  Maze m_maze(maze_w);      //マウスの持つ迷路データ
  StopWatch stopwatch;

  //迷路データの読み込み
  for(int i=0;i<maze_h;++i){
    for(int j=0;j<maze_w;++j){
      char chr;
      uint8_t cell=0;
      std::cin>>chr;
      if(chr>='0'&&chr<='9'){
        cell=chr-'0';
      }
      else if(chr>='a'&&chr<='f'){
        cell=chr+0xa-'a';
      }
      else{
        std::cout<<"Invalid maze data"<<std::endl;
        return 1;
      }
      dat_maze.addCell(cell);
    }
  }

  Viewer* dat_viewer=new Viewer(dat_maze);	
  Viewer* m_viewer=new Viewer(m_maze);
  dat_viewer->show();

  //マウスが持つ迷路もデータの初期化
  for(int i=0;i<maze_h;++i){
    for(int j=0;j<maze_w;++j){
      uint8_t cell=0;
      if(i==0)	cell |= NORTH;
      if(j==0)	cell |= WEST;
      if(j==(maze_w-1)) cell |= EAST;
      if(i==(maze_h-1)) cell	|= SOUTH;

      m_maze.addCell(cell);
    }
  }
  m_viewer->append(m_maze);
  m_viewer->show();

  //マウスの現在位置
  int mouse_index=start;

  m_maze.setChkWallAndWall(mouse_index,dat_maze.getWallData(start));
  m_viewer->append(m_maze);
  m_viewer->show();
  while(mouse_index!=goal){
    Graph graph(m_maze);
    graph.dijkstra(mouse_index,goal,searching);
    const Route	route=graph.getRoute(mouse_index,goal);
    m_viewer->append(m_maze);
    m_viewer->append(route);
    dat_viewer->append(route);
    m_viewer->show(mouse_index);
    //for(Route::const_reverse_iterator it=route.rbegin();it!=route.rend();++it) std::printf("%3d\n",*it);	
    for(Route::const_reverse_iterator it = (route.rbegin()+1);it!=route.rend();++it){
      uint8_t next_mouse_index=*(it+1);	
      mouse_index=*it;
      m_maze.setChkWallAndWall(mouse_index,dat_maze.getWallData(mouse_index));
      m_viewer->append(m_maze);
      m_viewer->append(route);
      dat_viewer->append(route);
      m_viewer->show(mouse_index);
      if(mouse_index==goal) break;
      if(next_mouse_index>mouse_index){
        if(((next_mouse_index-mouse_index)==1) && m_maze.getWallData(mouse_index)&EAST) break;
        else if((next_mouse_index-mouse_index)==maze_h && (m_maze.getWallData(mouse_index)&SOUTH)) break;
      }
      else{
        if(((mouse_index-next_mouse_index)==1) && (m_maze.getWallData(mouse_index)&WEST)) break;
        else if((mouse_index-next_mouse_index==maze_h) && (m_maze.getWallData(mouse_index)&NORTH)) break;
      }
    }
  }
  m_viewer->append(m_maze);
  m_viewer->show();
  //std::cout<<"searching is done"<<std::endl;

  //探索終了
  //maze_index=goal
  //delete m_viewer;
  //delete dat_viewer;
  //return 0;
  //goal to start
  while(mouse_index!=start){
    Graph graph(m_maze);
    graph.dijkstra(mouse_index,start,searching);
    const Route route=graph.getRoute(mouse_index,start);
    m_viewer->append(m_maze);
    m_viewer->append(route);
    dat_viewer->append(route);
    m_viewer->show(mouse_index);

    for(Route::const_reverse_iterator it=(route.rbegin()+1);it<route.rend();++it){
      uint8_t next_mouse_index=*(it+1);
      mouse_index=*it;
      m_viewer->append(m_maze);
      m_viewer->append(route);
      dat_viewer->append(route);
      m_viewer->show(mouse_index);
      m_maze.setChkWallAndWall(mouse_index,dat_maze.getWallData(mouse_index));
      if(mouse_index==start) break;
      if(next_mouse_index>mouse_index){
        if(((next_mouse_index-mouse_index)==1)&&(m_maze.getWallData(mouse_index)&EAST)) break;
        else if(((next_mouse_index-mouse_index)==maze_h) && (m_maze.getWallData(mouse_index)&SOUTH)) break;
      }
      else{
        if(((mouse_index-next_mouse_index)==1) && (m_maze.getWallData(mouse_index)&WEST)) break;
        else if(((mouse_index-next_mouse_index)==maze_h) && (m_maze.getWallData(mouse_index)&NORTH)) break;
      }
    }
  }
  //mause_index=start

  Viewer* viewer=new Viewer(m_maze);
  stopwatch.start();
  Graph graph(m_maze);
  graph.dijkstra(start,goal,solution);
  stopwatch.stop();
  viewer->append(graph.getRoute(start,goal));
  viewer->show();
  stopwatch.show();

  delete viewer;
  delete m_viewer;
  delete dat_viewer;
  return 0;
}

#include <algorithm>
#include <unistd.h>

#include "viewer.h"

const char* Viewer::DIRCHR=" ^> v   <";

Viewer::Viewer(){
	maze=new Maze(0);
}

Viewer::Viewer(const Maze& _maze){
	maze=new Maze(_maze);
}

Viewer::Viewer(const Maze& _maze,const Route& _route){
	maze=new Maze(_maze);
}


void Viewer::append(const Maze& _maze){
	delete maze;
	maze=new Maze(_maze);
}

void Viewer::append(const Route& _route){
	route=_route;
}

void Viewer::show() const{
  usleep(1000000);
  int16_t cursor_x=0;
	int16_t cursor_y=0;

	uint8_t maze_size=maze->getMazeSize();
	bool routeflag=(route.size()>0);

	std::cout<<"_";
	for(uint8_t i=0;i<maze_size;++i) std::cout<<"__";
	std::cout<<std::endl;

	MazeData m_data=maze->getMazeData();

	for(MazeData::const_iterator it=m_data.begin();it!=m_data.end();++it){
		int16_t index=cursor_x+maze_size*cursor_y;
		if(cursor_x==0) std::cout<<"|";

		if((*it)&SOUTH) std::cout<<"\x1b[4m";
		
		if((routeflag)&&(index==(*(route.begin())))){
			std::cout<<"G";
		}
		else if((routeflag)&&(index==(*(route.end()-1)))){
			std::cout<<"m";
		}
		else if(std::find(route.begin(),route.end(),index)!=route.end()){
			std::cout<<"*";
		}
		else{
			std::cout<<" ";
		}
		std::cout<<"\x1b[0m";

		if((*it)&EAST){ 
			std::cout<<"|";
		}
		else if((*it)&SOUTH){
			std::cout<<"_";
		}
		else{
			std::cout<<" ";
		}
		++cursor_x;
		if(cursor_x==maze_size){
			cursor_x=0;
			++cursor_y;
			std::cout<<std::endl;
		}
	}
}

void Viewer::show(int mouse_index) const{
  usleep(1000000);
  int16_t cursor_x=0;
	int16_t cursor_y=0;

	uint8_t maze_size=maze->getMazeSize();
	bool routeflag=(route.size()>0);

	std::cout<<"_";
	for(uint8_t i=0;i<maze_size;++i) std::cout<<"__";
	std::cout<<std::endl;

	MazeData m_data=maze->getMazeData();

	for(MazeData::const_iterator it=m_data.begin();it!=m_data.end();++it){
		int16_t index=cursor_x+maze_size*cursor_y;
		if(cursor_x==0) std::cout<<"|";

		if((*it)&SOUTH) std::cout<<"\x1b[4m";
		
		if((routeflag)&&(index==(*(route.begin())))){
			std::cout<<"G";
		}
		else if((routeflag)&&(index==mouse_index)){
			std::cout<<"m";
		}
		else if(std::find(route.begin(),route.end(),index)!=route.end()){
			std::cout<<"*";
		}
		else{
			std::cout<<" ";
		}
		std::cout<<"\x1b[0m";

		if((*it)&EAST){ 
			std::cout<<"|";
		}
		else if((*it)&SOUTH){
			std::cout<<"_";
		}
		else{
			std::cout<<" ";
		}
		++cursor_x;
		if(cursor_x==maze_size){
			cursor_x=0;
			++cursor_y;
			std::cout<<std::endl;
		}
	}
}


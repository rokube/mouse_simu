#pragma once

#include <vector>
#include <stdint.h>
#include <iostream>

enum Direction_t{
	NORTH=0x01,
	EAST=0x02,
	SOUTH=0x04,
	WEST=0x08,

	chk_NORTH=0x10,
	chk_EAST=0x20,
	chk_SOUTH=0x40,
	chk_WEST=0x80,
	chk_all=0xf0
};


struct Coord_t{
	uint8_t x;
	uint8_t y;
};

typedef std::vector<uint8_t> MazeData;

class Maze{
	private:
		MazeData maze_data;
		uint8_t maze_size;

	public:
		Maze(uint8_t _maze_size):maze_size(_maze_size){}
		Maze(const Maze& _maze):maze_data(_maze.maze_data),maze_size(_maze.maze_size){}
		~Maze(){
			maze_data.clear();
			//maze_data.shrink_to_fit();
			//std::cout<<"delete maze_data"<<std::endl;
		}
		void addCell(uint8_t data);
		uint8_t getMazeSize() const{ return maze_size; }
		const MazeData& getMazeData() const{ return maze_data; }
		const uint8_t getWallData(uint8_t index) const{ return maze_data[index];}
		const uint8_t getWallData(Coord_t coord) const{ return maze_data[coord.y*(maze_size)+coord.x];}
		void setWall(const Coord_t& coord,uint8_t wall);
		void setWall(const uint16_t index,uint8_t wall);
		const uint8_t isChkWall(Coord_t coord,uint8_t chk_wall) const;
		const uint8_t getChkWall(Coord_t coord) const;
		void setChkWall(Coord_t coord);
		void setChkWall(uint16_t index);
		void setChkWallAndWall(Coord_t coord,uint8_t wall);
		void setChkWallAndWall(uint16_t index,uint8_t wall);
};



#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <cstdio>
#include "graph.h"
#include "maze.h"

void Node::addEdge(int16_t to,int16_t _cost,bool found){
	Edge edge;
	edge.to=to;
	edge.cost=_cost;
	edge.found=found;
	edges.push_back(edge);
}

void Node::deleteEdge(int16_t to){
	for(Edges::iterator it=edges.begin();it!=edges.end();){
		if(it->to==to){
			it=edges.erase(it);
			continue;
		}
		++it;
	}
}

void Node::setCostOfEdge(int16_t to,int16_t cost){
	for(Edges::iterator it=edges.begin();it!=edges.end();++it){
		if((it->to==to)&&((it->cost)<cost))
			it->setCost(cost);
	}
}

void Node::print_node() const{
	printf("index:%d, done:%s, cost:%d, from:%d\nedges:\n",index,(done?"true":"false"),cost,from_node);

	for(Edges::const_iterator it=edges.begin();it!=edges.end();++it){
		//std::cout<<"cost:"<<it->cost<<",to:"<<it->to<<std::endl;
		printf("cost: %d ,to: %d\n",it->cost,it->to);
	}

}

Graph::Graph(const Maze& maze){
	maze_size=maze.getMazeSize();
	int16_t cnt=0;
	const MazeData& maze_data=maze.getMazeData();
	//std::cout<<"graph start"<<std::endl;
	//show_maze(maze);
	nodes.reserve(256);
	for(MazeData::const_iterator it=maze_data.begin();it!=maze_data.end();++it){
		addNode(cnt,-1);
		int16_t cost=1;
		//std::cout<<"index:"<<cnt<<std::endl<<"wall:"<<(*it)<<std::endl;
		//std::printf("index: %d wall: %x\n",cnt,(*it));	
		if(!(((*it) & NORTH)==NORTH)){
			if((*it)&SOUTH) cost+=2;
			if((cnt-maze_size>=0)&&(maze_data[cnt-maze_size] & NORTH)) cost+=2;

			nodes[cnt-maze_size].setCostOfEdge(cnt,cost);
			addDirectionalEdge(cnt,cnt-maze_size,cost,(*it&chk_NORTH)==chk_NORTH);
		}
		cost=1;
		if(!((*it)&EAST)){
			if((*it)&WEST) cost+=2;
			if((cnt+1<256)&&(maze_data[cnt+1] & EAST)) cost+=2;
			addDirectionalEdge(cnt,cnt+1,cost,(*it&chk_EAST)==chk_EAST);
		}
		cost=1;
		if(!(((*it) & SOUTH)==SOUTH)){
			if(!((*it) & NORTH)) cost+=2;
			if((cnt+maze_size<256)&&(maze_data[cnt+1]&EAST)) cost+=2;
			addDirectionalEdge(cnt,cnt+maze_size,cost,(*it&chk_SOUTH)==chk_SOUTH);
		}
		cost=1;
		if(!((*it) & WEST)){
			if((*it) & EAST) cost+=2;
			if((cnt-1>=0)&&(maze_data[cnt-1] & WEST))	cost+=2;
			nodes[cnt-1].setCostOfEdge(cnt,cost);
			addDirectionalEdge(cnt,cnt-1,cost,((*it)&chk_WEST)==chk_WEST);
		}
		++cnt;
	}
	//std::cout<<"make graph"<<std::endl;
}

void Graph::addNode(const uint16_t index,const int16_t _cost){
	Node node(index,_cost);
	nodes.push_back(node);
}

void Graph::addEdges(int16_t pos_a,int16_t pos_b,int16_t _cost,bool found){
	nodes[pos_a].addEdge(pos_b,_cost,found);
	nodes[pos_b].addEdge(pos_a,_cost,found);
}

void Graph::addDirectionalEdge(int16_t from,int16_t to,int16_t _cost,bool found){
	nodes[from].addEdge(to,_cost,found);
}

void Graph::deleteEdge(int16_t pos_a,int16_t pos_b){
	nodes[pos_a].deleteEdge(pos_b);
	nodes[pos_b].deleteEdge(pos_a);
}

void Graph::print_graph() const{
	int16_t cursor=0;

	for(Nodes::const_iterator it=nodes.begin();it!=nodes.end();++it){
		if(it->isDone()) std::cout<<"*";
		else std::cout<<"o";
		++cursor;

		if(cursor==maze_size){
			std::cout<<std::endl;
			cursor=0;
		}
	}
}

void Graph::dijkstra(int16_t start,int16_t goal,dijkstra_mode_t mode){
	nodes[start].setCost(0);

	std::priority_queue<Node*,std::vector<Node*>,std::greater<Node*> > node_queue;
	//std::priority_queue<Node*> node_queue;
	std::vector<int16_t>in_queue;
	node_queue.push(&nodes[start]);

	while(1){
		Node* doneNode=node_queue.top();
		node_queue.pop();

		for(std::vector<int16_t>::iterator it=in_queue.begin();it!=in_queue.end();){
			if((*it)==doneNode->getIndex()){
				it=in_queue.erase(it);
				continue;
			}
			++it;
		}
		//doneNode->setDone();
		//doneNode->print_node();
		Edges edges=doneNode->getEdges();
		for(Edges::iterator it=edges.begin();it!=edges.end();++it){
			int16_t to=it->to;
			int16_t cost=doneNode->getCost()+it->cost;
			if((it->found)||(mode==searching)){
				if(nodes[to].getCost()<0||cost<nodes[to].getCost()){
					nodes[to].setCost(cost);
					nodes[to].setFrom_node(doneNode->getIndex());
					if(find(in_queue.begin(),in_queue.end(),to)==in_queue.end()){
						node_queue.push(&nodes[to]);
						in_queue.push_back(to);
					}
				}
			}
		}
		if(doneNode->getIndex()==goal) break;
	}
}

const Route Graph::getRoute(int16_t start,int16_t end) const{
	int16_t cursor=end;
	Route route;

	while((cursor>=0)&&(cursor!=start)){
		route.push_back(cursor);
		cursor=nodes[cursor].getFrom_node();
	}
	route.push_back(start);

	return route;
}

void Graph::show_maze(const Maze& maze) const{
	int16_t cursor_x=0;
	int16_t cursor_y=0;

	/*std::cout<<"_";
	for(uint8_t i=0;i<maze_size;++i) std::cout<<"__";
	std::cout<<std::endl;

	MazeData m_data=maze.getMazeData();

	for(MazeData::const_iterator it=m_data.begin();it!=m_data.end();++it){
		//int16_t index=cursor_x+maze_size*cursor_y;
		if(cursor_x==0)std::cout<<"|";
		if((it->wall)&SOUTH) std::cout<<"\x1b[4m]";
		std::cout<<" ";
		std::cout<<"\x1b[0m";

		if((it->wall)&EAST){
			std::cout<<"|";
		}
		else if((it->wall)&SOUTH){
			std::cout<<"_";
		}
		else{
			std::cout<<" ";
		}
		++cursor_x;
		if(cursor_x==maze_size){
			cursor_x=0;
			++cursor_y;
			std::cout<<std::endl;
		}
	}*/

	MazeData m_data=maze.getMazeData();
	for(MazeData::const_iterator it=m_data.begin();it!=m_data.end();++it){
		printf("%3x",*it);
		++cursor_x;
		if(cursor_x==maze_size){
			cursor_x=0;
			std::cout<<std::endl;
		}
	}
}

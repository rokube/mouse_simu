#include <iostream>
#include <vector>
#include <cstdio>
#include "maze.h"

void Maze::addCell(uint8_t data){
	maze_data.push_back(data);
}

void Maze::setWall(const Coord_t& coord,uint8_t wall){
	maze_data[coord.y*maze_size+coord.x]|=wall;

	if((wall&NORTH)&&coord.y!=0){
		maze_data[(coord.y-1)*maze_size+coord.x]|=SOUTH;
	}
	if((wall&EAST)&&(coord.x!=maze_size-1)){
		maze_data[coord.y*maze_size+coord.x+1]|=WEST;
	}
	if((wall&SOUTH)&&(coord.y!=(maze_size-1))){
		maze_data[(coord.y+1)*maze_size+coord.x]|=NORTH;
	}
	if((wall&WEST)&&(coord.x!=0)){
		maze_data[coord.y*maze_size+coord.x-1]|=EAST;
	}
}

void Maze::setWall(const uint16_t index,uint8_t wall){
	maze_data[index]|=wall;
	
	if((wall&NORTH)&&(index+1>maze_size)){
		maze_data[index-maze_size]|=SOUTH;
	}
	if((wall&EAST)&&((index+1%maze_size)!=0)){
		maze_data[index+1]|=WEST;
		//std::printf("EAST index: %d , wall: %3x\n",index,wall);
	}
	if((wall&SOUTH)&&(index<(maze_size*(maze_size-1)))){
		maze_data[index+maze_size]|=NORTH;
	}
	if((wall&WEST)&&((index%maze_size)!=0)){
		maze_data[index-1]|=EAST;
		//std::printf("WEST index: %d ,wall: %3x\n",index,wall);
	}
}

const uint8_t Maze::isChkWall(Coord_t coord,uint8_t chk_wall) const{	
	return maze_data[coord.y*maze_size+coord.x]&chk_wall;
}

const uint8_t Maze::getChkWall(Coord_t coord) const{
	return maze_data[coord.y*maze_size+coord.x]&chk_all;
}

void Maze::setChkWall(Coord_t coord){
	maze_data[coord.y*maze_size+coord.x]|=chk_all;

	if(coord.y!=0){
		maze_data[(coord.y-1)*maze_size+coord.x]|=chk_SOUTH;
	}
	if(coord.x!=(maze_size-1)){
		maze_data[coord.y*maze_size+coord.x+1]|=chk_WEST;
	}
	if(coord.y!=(maze_size-1)){
		maze_data[(coord.y+1)*maze_size+coord.x]|=chk_NORTH;
	}
	if(coord.x!=0){
		maze_data[coord.y*maze_size+coord.x-1]|=chk_EAST;
	}
}

void Maze::setChkWall(uint16_t index){
	maze_data[index]|=chk_all;

	if((index+1)>maze_size){
		maze_data[index-maze_size]|=chk_SOUTH;
	}
	if((index+1%maze_size)!=0){
		maze_data[index+1]|=chk_WEST;
	}
	if(index<(maze_size*(maze_size-1))){
		maze_data[index+maze_size]|=chk_NORTH;
	}
	if((index%maze_size)!=0){
		maze_data[index-1]|=chk_EAST;
	}
}

void Maze::setChkWallAndWall(Coord_t coord,uint8_t wall){
	setWall(coord,wall);
	setChkWall(coord);
}

void Maze::setChkWallAndWall(uint16_t index,uint8_t wall){
	setWall(index,wall);
	setChkWall(index);
}


#pragma once

#include <stdint.h>
#include <vector>
#include "graph.h"
#include "maze.h"
#include "viewer.h"

class Mouse{
  private:
    Coord_t coord;
    Maze* maze;
    uint8_t maze_size;
  public:
    static const long STEP_TIME=50000;
  protected:
    const char* dir_char=" ^> v  <";

  public:
    Mouse(const Maze& new_maze,int16_t width);
    ~Mouse(){delete maze;}
    void sense_set_wall(const Maze& real_maze);
    uint8_t sense_front(const Maze& real_maze) const;
    uint8_t sense_right(const Maze& real_maze) const;
    uint8_t sense_left(const Maze& real_maze) const;
    uint8_t move_forward();
    void turn_right();
    void turn_left();
    const Coord_t get_coord() const{return coord;}
    const int16_t get_coord_index() const{ return coord.y*maze_size+coord.x;}
    const Maze& getMaze() const{return *(maze);}
    void set_maze(const Maze& new_maze);
    void set_wall(uint8_t dir);
    void clear_maze();
    uint8_t trans_direction(const uint8_t local) const;
    const Route search_maze(Viewer* viewer,Maze& real_maze,uint8_t maze_w,int16_t start,int16_t goal,dijkstra_mode_t is_searching);
    void get_unsearched_cell(std::vector<int16_t>& v,int16_t start,int16_t goal) const;
};

#pragma once
#include <vector>
#include <stdint.h>

#include "graph.h"
#include "maze.h"


class Viewer{
	private:
		Maze *maze;
		Route route;
		static const char *DIRCHR;

	public:
		Viewer();
		Viewer(const Maze& _maze);
		Viewer(const Maze& _maze,const Route& _route);
		
		~Viewer(){ delete maze;}

		void append(const Maze& _maze);
		void append(const Route& _route);

		void clear_maze();
		void clear_route();
    void show(int mouse_index) const;
		void show() const;
};


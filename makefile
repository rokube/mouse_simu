CXX=g++
OBJS=graph.o maze.o viewer.o stopwatch.o

all: dijkstra

dijkstra: main.o $(OBJS)
	$(CXX) $^ -o $@

main.o: main.cpp
	$(CXX) -c -g -Wall $<

maze.o: maze.cpp
	$(CXX) -c -g -Wall $<

graph.o: graph.cpp
	$(CXX) -c -g -Wall $<

viwer.o: viewr.cpp
	$(CXX) -c -g -Wall $<

stopwatch.o: stopwatch.cpp
	$(CXX) -c -g -Wall $<

clean: 
	rm -f *.o *.out

